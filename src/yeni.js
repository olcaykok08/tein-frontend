<Form inline>
  <FormGroup>
    <Label for="exampleEmail" hidden>
      Email
    </Label>
    <Input style={{ alignItems: "center", width: "30%" }}>
      name="username" onChange={this.onChange}
      placeholder="username"
    </Input>
  </FormGroup>{" "}
  <FormGroup>
    <Label for="examplePassword" hidden>
      Password
    </Label>
    <Input
      onChange={this.onChange}
      style={{
        width: "80%",
        margin: "10px",
        alignItems: "center",
      }}
      name="password"
      placeholder="password"
    />
  </FormGroup>{" "}
  <Button style={{ margin: "10px", background: "blue" }} onClick={this.onClick}>
    Submit
  </Button>
</Form>;
