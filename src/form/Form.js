import React,{Component} from "react";
import {
  Label,
  Col,
  Form,
  FormGroup,
  Input,
  Button,
  Row,
  Container,
} from "reactstrap"; 

export default class form1 extends Component {
  render() {
    return (
      <div className="App">
      <Row>
        <Col style={{ "text-align": "center" }}>
          <h2>CV Hazırlama </h2>
        </Col>
      </Row>
      <Container>
        <Form>
          <FormGroup row>
            <Label for="exampleName" sm={2}>
              Name
            </Label>
            <Col sm={10}>
              <Input
                id="exampleName"
                name="Name"
                placeholder="with a placeholder"
              />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="exampleSurname" sm={2}>
              Surname
            </Label>
            <Col sm={10}>
              <Input
                id="exampleSurname"
                name="surname"
                placeholder="with a placeholder"
              />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="exampleNumber" sm={2}>
              Phone
            </Label>
            <Col sm={10}>
              <Input
                id="exampleNumber"
                name="number"
                placeholder="with a placeholder"
                type="number"
              />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="exampleEmail" sm={2}>
              Email
            </Label>
            <Col sm={10}>
              <Input
                id="exampleEmail"
                name="email"
                placeholder="with a placeholder"
                type="email"
              />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="exampleEducation" sm={2}>
              Education
            </Label>
            <Col sm={10}>
              <Input
                id="exampleEducation"
                name="education"
                placeholder="password placeholder"
                type="select"
              >
                <option>Abant İzzet Baysal Üniversitesi</option>
                <option>İstanbul Teknik Üniversitesi</option>
                <option>Düzce Üniversitesi</option>
                <option>Giresun Üniversitesi</option>
                <option>Marmara Üniversitesi</option>
              </Input>
            </Col>
          </FormGroup>
          <FormGroup>
            <Row>
              <Label for="exampleLisans" sm={2}>
                Period
              </Label>
              <Col sm={10}>
                <Row>
                  <Col md={6}>
                    <FormGroup>
                      <Label for="exampleStart">Kayıt Tarihi</Label>
                      <Input id="exampleStart" name="start" type="select">
                        <option>2022</option>
                        <option>2021</option>
                        <option>2020</option>
                        <option>2019</option>
                        <option>2018</option>
                        <option>2017</option>
                        <option>2016</option>
                        <option>2015</option>
                        <option>2014</option>
                        <option>2013</option>
                        <option>2012</option>
                      </Input>
                    </FormGroup>
                  </Col>
                  <Col md={4}>
                    <FormGroup>
                      <Label for="exampleYear">Mezuniyet Yılı</Label>
                      <Input id="exampleYear" name="state" type="select">
                        <option>2019</option>
                      <option>2020</option>
                        <option>2021</option>
                        <option>2022</option>
                        <option>2023</option>
                        <option>2024</option>
                        <option>2025</option>
                        <option>2026</option>
                      </Input>
                    </FormGroup>
                  </Col>
                </Row>
              </Col>
            </Row>
          </FormGroup>
          <Row>
            <Col style={{ "text-align": "center" }}>
              <h4>Experience</h4>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="Company">Şirket Adı</Label>
                <Input id="Company" name="company" type="select">
                  <option>Lütfen Seçin</option>
                  <option>Tein Yazilim Hizmetleri Ticaret A.Ş.</option>
                  <option>Hira Parl Yazılım Bilişim</option>
                  <option>Bilişim Vadisi</option>
                  <option>Penta Teknoloji</option>
                  <option>Teknosa</option>
                </Input>
              </FormGroup>
            </Col>
            <Col md={6}>
              <FormGroup>
                <Label for="examplePassword">Ünvan</Label>
                <Input id="examplePassword" name="password" type="select">
                  <option>Örn: Parekende Satış Müdürü</option>
                  <option>Intern</option>
                  <option>CTO</option>
                  <option>Software Development</option>
                  <option>CEO</option>
                </Input>
              </FormGroup>
            </Col>
          </Row>
          <Col md={6}>
            <FormGroup>
              <Label for="exampleAddress">Address</Label>
              <Input id="exampleAddress" name="adress" type="select">
                <option>Örn: İstanbul,Türkiye</option>
                <option>Düzce,Türkiye</option>
                <option>Londra,İngiltere</option>
                <option>Giresun,Türkiye</option>
                <option>Pariss,Fransa</option>
              </Input>
            </FormGroup>
          </Col>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="exampleStart">Başlama Tarihi</Label>
                <Input id="exampleStart" name="start" type="select">
                  <option>Ay</option>
                  <option>Ocak</option>
                  <option>Şubat</option>
                  <option>Mart</option>
                  <option>Mart</option>
                  <option>Nisan</option>
                  <option>Mayıs</option>
                  <option>Haziran</option>
                  <option>Temmuz</option>
                  <option>Ağustos</option>
                  <option>Eylül</option>
                  <option>Ekim</option>
                  <option>Kasım</option>
                  <option>Aralık</option>
                </Input>
              </FormGroup>
            </Col>
            <Col md={4}>
              <FormGroup>
                <Label for="exampleYear">Year</Label>
                <Input id="exampleYear" name="state" type="select">
                  <option>2022</option>
                  <option>2021</option>
                  <option>2020</option>
                  <option>2019</option>
                  <option>2018</option>
                  <option>2017</option>
                  <option>2016</option>
                  <option>2015</option>
                  <option>2014</option>
                  <option>2013</option>
                  <option>2012</option>
                </Input>
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col md={6}>
              <FormGroup>
                <Label for="exampleStart">Bitiş Tarihi</Label>
                <Input id="exampleStart" name="start" type="select">
                  <option>Ay</option>
                  <option>Ocak</option>
                  <option>Şubat</option>
                  <option>Mart</option>
                  <option>Mart</option>
                  <option>Nisan</option>
                  <option>Mayıs</option>
                  <option>Haziran</option>
                  <option>Temmuz</option>
                  <option>Ağustos</option>
                  <option>Eylül</option>
                  <option>Ekim</option>
                  <option>Kasım</option>
                  <option>Aralık</option>
                </Input>
              </FormGroup>
            </Col>
            <Col md={4}>
              <FormGroup>
                <Label for="exampleFinish">Year</Label>
                <Input id="exampleFinish" name="state" type="select">
                  <option>2022</option>
                  <option>2021</option>
                  <option>2020</option>
                  <option>2019</option>
                  <option>2018</option>
                  <option>2017</option>
                  <option>2016</option>
                  <option>2015</option>
                  <option>2014</option>
                  <option>2013</option>
                  <option>2012</option>
                </Input>
              </FormGroup>
            </Col>
          </Row>

          <FormGroup check row>
            <Col
              sm={{
                offset: 2,
                size: 10,
              }}
            >
              <Button>Submit</Button>
            </Col>
          </FormGroup>
        </Form>
      </Container>
    </div>
    )
  }
}
 