import { Container, Form, Table } from "reactstrap";
import { Component } from "react";
import "./Admin.css"
class AdminPage extends Component {
  render() {
    return (
      <div>
        <Form>
            <h2 className="Admin-Header">CV ler</h2>
          <Container >
            <Table bordered className="Admin-Table">
              <thead>
                <tr>
                  <th>id</th>
                  <th>Name</th>
                  <th>Surname</th>
                  <th>e-mail</th>
                  <th>Tel:</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <link></link>
                  <th scope="row">1.</th>
                  <td>Olcay</td>
                  <td>KÖK</td>
                  <td>olcaykok08@gmail.com</td>
                  <td>05380810228</td>
                  <td>
                    <button onClick={{}} className="Admin-Button">Ayrıntı</button>
                  </td>
                </tr>
                <tr>
                  <th scope="row">2.</th>
                  <td>Nurdan</td>
                  <td>SEREN</td>
                  <td>cell@gmail.com</td>
                  <td>05632545574</td>
                </tr>
                <tr>
                  <th scope="row">3.</th>
                  <td>Ozan</td>
                  <td>ÇELİK</td>
                  <td>sfgsdf@gmail.com</td>
                  <td>05632545574</td>
                </tr>
                <tr>
                  <th scope="row">4.</th>
                  <td>Cihan</td>
                  <td>KÖK</td>
                  <td>dfgdf@gmail.com</td>
                  <td>05667987889</td>
                </tr>
                <tr>
                  <th scope="row">5.</th>
                  <td>Kemal</td>
                  <td>YAŞAR</td>
                  <td>dfgfd@gmail.com</td>
                  <td>05665768476</td>
                </tr>
              </tbody>
            </Table>
          </Container>
        </Form>
      </div>
    );
  }
}

export default AdminPage;
