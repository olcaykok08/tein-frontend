import React from "react";
import LoginPage from "./login/Login";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="App">
      <LoginPage></LoginPage>
    </div>
  );
}

export default App;
