import React from "react";
import { Input, Row, Col, Button, Container, Form } from "reactstrap";
import axios from "axios";
import "./Login.css";

class Login extends React.Component {
  state = {
    username: null,
    password: null,
  };

  onChange = (event) => {
    const { value, name } = event.target;
    this.setState({
      [name]: value,
    });
  };
  onClick = (event) => {
    event.preventDefault();

    const { username, password } = this.state;
    const body = {
      username,
      password,
    };

    axios
      .post("http://localhost:8080/public-driverapp-api/login", body)
      .then((response) => {});
  };

  render() {
    return (
      <div>
        <Container className="Login-Container">
          <Row>
            <Col className="Login-Header">
              <h2>CV Projesine Hoşgeldiniz</h2>
            </Col>
          </Row>

          <Row>
            <Form>
              <Row className="Login-Col">
                <Col>
                  <Input
                    className="Login-Input"
                    name="username"
                    onChange={this.onChange}
                    placeholder="username"
                  />
                </Col>
              </Row>
              <Row className="Login-Col">
                <Col>
                  <Input
                    className="Login-Input"
                    onChange={this.onChange}
                    name="password"
                    placeholder="password"
                  />
                </Col>
              </Row>
              <Button className="Login-Button" onClick={this.onClick}>
                Submit
              </Button>
            </Form>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
